## About you

(Describe shortly who you are, if you're working for an canton, a city, a company, ...)

## The new feature

(A clear and concise description of what feature you would like to see in GeoGirafe. If necessary, add some screenshots.)

## Additional information or points to watch out for

(Any other information about this feature request, or any point to watch out for or open question)

/label ~draft
/label ~new
