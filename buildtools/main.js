export { default as InlineTemplatesPlugin } from './vite-inline-templates-plugin.js';
export { default as HtmlRebuildPlugin } from './vite-restart-plugin.js';
